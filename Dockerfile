FROM python:3.9-slim

WORKDIR /project
RUN pip3 install flask
COPY hello.py .
ENV FLASK_APP=hello
CMD ["flask", "run", "--host=0.0.0.0", "--port=5000"]
