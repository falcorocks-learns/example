# Gitlab Environments

* `flask.py` deploys a minimal flask app that prints Hello World + the commit hash
* environments are [here](https://gitlab.com/falcorocks-learns/example/-/environments)
* `production` environment: deploys `main` branch to https://prod.example.falcorocks.com
* branches:
    - each branch gets a new environment called `review/$CI_COMMIT_REF_SLUG`
    - environment can be deleted manually (for example after merging). Resources are destroyed on the server when the environment is deleted.
    - each branch deploys to https://$CI_COMMIT_REF_SLUG.example.falcorocks.com
    - branch `staging-fix-something` will deploy to https://staging-fix-something.example.falcorocks.com
* one perk of environments is that gitlab will not perform a deployment if there's a more recent commit on the same branch

TODOS:
* add healtcheck
